package com.ike.articlesme.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ike.articlesme.R
import com.ike.articlesme.databinding.ActivityWelcomeBinding
import com.ike.articlesme.ui.splash.SplashFragment

class WelcomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWelcomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val splashFragment = SplashFragment()

        val existingFragment = supportFragmentManager.findFragmentByTag("splash_fragment_tag")

        if (existingFragment != null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.nav_host_fragment, splashFragment, "splash_fragment_tag")
                .commit()
        } else {
            supportFragmentManager.beginTransaction()
                .add(R.id.nav_host_fragment, splashFragment, "splash_fragment_tag")
                .commit()
        }
    }
}