package com.ike.articlesme.ui.data

import android.os.Parcel
import android.os.Parcelable

data class NewsItem(
    val title: String,
    val description: String?,
    val url: String,
    val imageUrl: String,
    val publishedAt: String,
    val content: String?,
): Parcelable {
    constructor(parcel: Parcel) : this(
    parcel.readString()?:"",
    parcel.readString(),
    parcel.readString()?:"",
    parcel.readString()?:"",
    parcel.readString()?:"",
    parcel.readString()?:""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(url)
        parcel.writeString(imageUrl)
        parcel.writeString(publishedAt)
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewsItem> {
        override fun createFromParcel(parcel: Parcel): NewsItem {
            return NewsItem(parcel)
        }

        override fun newArray(size: Int): Array<NewsItem?> {
            return arrayOfNulls(size)
        }
    }
}
