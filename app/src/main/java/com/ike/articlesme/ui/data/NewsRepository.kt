package com.ike.articlesme.ui.data

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewsRepository {
    private val service: NewsService = Retrofit.Builder()
        .baseUrl("https://newsapi.org/v2/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(NewsService::class.java)

    suspend fun getTopHeadlines(): List<NewsItem> {
        val response = service.getTopHeadlines(apiKey = "cff06e8cb11c428195acfc71d2c27aab")
        return response.articles.map {
            NewsItem(
                it.title,
                it.description,
                it.url,
                it.urlToImage ?: "",
                it.publishedAt,
                it.content ?: ""
            )
        }
    }
}