package com.ike.articlesme.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.ike.articlesme.R
import com.ike.articlesme.databinding.FragmentDetailNewsBinding
import com.ike.articlesme.ui.utils.asDateFormatter

class DetailNewsFragment : Fragment() {

    private lateinit var binding: FragmentDetailNewsBinding
    private val args: DetailNewsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailNewsBinding.inflate(layoutInflater)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val newsItem = args.data
        val dateFormat = asDateFormatter(newsItem.publishedAt).toString()

        binding.apply {
            val icon = R.drawable.ic_arrow_back_circle
            toolbar.backHeader.setImageResource(icon)
            toolbar.backHeader.setOnClickListener { findNavController().popBackStack() }
            toolbar.titleHeader.text = getString(R.string.detail_article)

            tvTitle.text = newsItem.title
            tvDescription.text = newsItem.description ?: getString(R.string.no_description_available)
            Glide.with(imageView.context)
                .load(newsItem.imageUrl)
                .into(imageView)

            tvDateTime.text = dateFormat
            tvContent.text = newsItem.content ?: getString(R.string.no_content_available)

            btnReadMore.setOnClickListener {
                if (newsItem.url.isBlank()) {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.link_not_available),
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    openWebPage(newsItem.url)
                }
            }
        }
    }

    private fun openWebPage(url: String) {
        val webpage = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(webpage)
    }

}
