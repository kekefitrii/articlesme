package com.ike.articlesme.ui.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.ike.articlesme.R
import com.ike.articlesme.databinding.FragmentNewsBinding
import com.ike.articlesme.ui.data.NewsItem

class NewsFragment : Fragment() {

    private val viewModel: NewsViewModel by viewModels()
    private lateinit var adapter: NewsAdapter
    private lateinit var binding: FragmentNewsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchNews()
        initView()
    }

    private fun initView() {
        binding.toolbar.titleHeader.text = getString(R.string.app_name)

        adapter = NewsAdapter { newsItem -> navigateTODetail(newsItem) }
        binding.recyclerView.adapter = adapter

        viewModel.newsList.observe(viewLifecycleOwner) { newsList ->
            adapter.submitList(newsList)
        }
    }

    private fun navigateTODetail(newsItem: NewsItem) {
        val navController = findNavController()

        val data = NewsItem(
            title = newsItem.title,
            description = newsItem.description,
            url = newsItem.url,
            imageUrl = newsItem.imageUrl,
            publishedAt = newsItem.publishedAt,
            content = newsItem.content
        )

        val action = NewsFragmentDirections.actionNewsFragmentToDetailNewsFragment(data)
        navController.navigate(action)
    }

}