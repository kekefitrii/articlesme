package com.ike.articlesme.ui.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ike.articlesme.ui.data.NewsItem
import com.ike.articlesme.ui.data.NewsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsViewModel : ViewModel() {
    private val _newsList = MutableLiveData<List<NewsItem>>()
    val newsList: LiveData<List<NewsItem>> = _newsList

    fun fetchNews() {
        CoroutineScope(Dispatchers.IO).launch {
            val news = NewsRepository().getTopHeadlines()
            _newsList.postValue(news)
        }
    }
}