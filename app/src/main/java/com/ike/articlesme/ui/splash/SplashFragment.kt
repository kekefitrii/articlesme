package com.ike.articlesme.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.ike.articlesme.R
import com.ike.articlesme.databinding.FragmentSplashBinding
import com.ike.articlesme.ui.data.NewsItem

class SplashFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Handler(Looper.getMainLooper()).postDelayed({
            navigateToNewsFragment()
        }, SPLASH_SCREEN_DURATION)
    }

    private fun navigateToNewsFragment() {
        if (findNavController().currentDestination?.id == R.id.splashFragment) {
            val action = SplashFragmentDirections.actionSplashFragmentToNewsFragment()
            findNavController().navigate(action)
        }
    }

    companion object {
        const val SPLASH_SCREEN_DURATION: Long = 1000
    }
}