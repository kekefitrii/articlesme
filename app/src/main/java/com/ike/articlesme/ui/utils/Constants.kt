package com.ike.articlesme.ui.utils

object Constants {

    const val DATE_FORMAT = "dd MMM yyyy HH:mm"
}