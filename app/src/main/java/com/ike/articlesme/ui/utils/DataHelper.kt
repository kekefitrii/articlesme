package com.ike.articlesme.ui.utils

import android.os.Build
import androidx.annotation.RequiresApi
import com.ike.articlesme.ui.utils.Constants.DATE_FORMAT
import java.io.Serializable
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

@RequiresApi(Build.VERSION_CODES.O)
fun LocalDateTime.formatToCustomPattern(pattern: String): String {
    val formatter = DateTimeFormatter.ofPattern(pattern)
    return this.format(formatter)
}

@RequiresApi(Build.VERSION_CODES.O)
fun asDateFormatter(epochTimestamp: String): Serializable {

    val res = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val formatter = DateTimeFormatter.ISO_DATE_TIME
        val dateTime = LocalDateTime.parse(epochTimestamp, formatter)
        dateTime.formatToCustomPattern(DATE_FORMAT)

    } else {
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        val dateTime = LocalDateTime.parse(epochTimestamp, formatter)
        dateTime.format(formatter)
    }
    return res
}